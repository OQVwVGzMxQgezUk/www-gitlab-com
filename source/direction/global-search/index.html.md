---
layout: markdown_page
title: "Category Direction - Global Search"
description: "GitLab supports Advanced Search for GitLab.com and Self-Managed instances. This provides users with a faster and more complete search."
canonical_path: "/direction/global-search/"
---

- TOC
{:toc}

## Global Search

| | |
| --- | --- |
| Section | [Enablement](/direction/enablement/) |
| Content Last Reviewed | `2021-01-24` |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

Thank you for visiting this direction page on Global Search in GitLab. This page belongs to the [Global Search](/handbook/product/categories/#global-search-group) group of the Enablement stage and is maintained by John McGuire ([E-Mail](mailto:jmcguire@gitlab.com)).

This strategy evolves, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AGlobal%20Search) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AGlobal%20Search) on this page. Sharing your feedback directly on GitLab.com or submitting a Merge Request to this page are the best ways to contribute to our direction.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for search, we'd especially love to hear from you.

### Overview
<!-- A good description of what your category is. If there are special considerations for your strategy or how you plan to prioritize, the description is a great place to include it. Please include use cases, personas, and user journeys into this section. -->

Global Search is the core search feature for GitLab, as the one place to search for anything in the platform. “Global” refers to the ability to search at the project, group, or instance level, as well as across content types. Users can search issues, merge requests, commits, epics, code, projects, wikis, comments, and users.

There are two modes Global Search can operate in, depending on the instance configuration:

* Basic Search is the default search mode for a new instance of GitLab, requiring no additional configuration. It uses PostgreSQL and allows for some limited searching across groups and projects. Cross-project code search is not supported.
* Advanced Search is available at the Starter/Bronze tier, and provides a richer search experience. An Elasticsearch cluster is required, which is not packaged by GitLab today. Advanced Search allows cross-project and group code search, is faster than Basic Search, and provides a richer search syntax.  

The most frequent content type searched is code, at over 40% of queries. 

### Vision - Where we are headed

A modern search user interface that will allow users to navigate through their search to filter down to what they are looking for. Offering comprehensive filter options will allow users to search with vague context, decide what applies to what they are looking for, and filter out everything else. Making these filters dynamic based on the information returned from the initial query will encourage users to explore the information in GitLab easier. 

Advanced Search uses Elasticsearch to index and search GitLab.  Elasticsearch is an open-source search engine that has a wide range of uses that go far beyond traditional keyword-based searching. Having this information available in a very quick index may make integrations with SAST and DAST more flexible. There are also opportunities to connect to code intelligence and enhance context used in code when searching.  

#### Challenges to address

* Scaling Elasticsearch is a constant struggle. Elasticsearch clusters are designed to scale however we are growing so quickly that our SaaS cluster requires frequent optimizations. Self-managed users run a variety of scales and configuring optimizations for every size in a one-configuration-fits-all is not easy with Elasticsearch. Ultimately we will need to [change the architecture](https://gitlab.com/gitlab-org/gitlab/-/issues/296503) to sustain long-term scalability. 
* Scaling requires a lot of storage. We have done decreased the index size by >80%, however, as we index additional content types and fields, it will grow at a higher proportion to the corpus size. 
* Ranking and Relevancy will need to use very generalized modifications due to how users use GitLab, and we will need to use non-standard methods of adapting to popularity. 
* Machine Learning is a highly valuable feature in Elasticsearch, which we have yet to explore how this can benefit Global Search as well as other stages of GitLab.

### 2021 Goals FY22 / One-year plan

- Breakout the Index by scopes so that scaling is easier to manage and queries run faster
- Move the UI over to Vue to make adding filters and options easier
- Re-architect for efficiency and move to managing our own stack
- Add new filters and features
- Federated Search for GitLab Self-managed and GitLab.com 
- Integration framework for adding content from other products like JIRA
- Solve for elastic scaling that can handle continuous growth
- Index diffs and all branches
- Add saved searches and create Alerting for these searches 
- Improve documentation and installation options for self-managed users 

### Desired Results

Global Search should be used frequently by every user that uses GitLab. It should be created in a way that allows users to build their own process and become the core of an internal way to communicate and share contributions. 

The Elasticsearch Integration should become the primary piece to the GitLab product and serve out every list page allowing for more interactive features and keyword search. 

We should have a tight connection with the community and customers to provide shared details, and encourage contributions. 

### Measuring Success - Target PI's / Metrics

We look at unique users per month, GMAU.  Our goal is to grow this 10% each month. 

GitLab currently supports [Advanced Search](https://docs.gitlab.com/ee/user/search/advanced_global_search.html) for Starter and above self-managed instances. This provides users with a faster and more complete search experience across GitLab. GitLab.com similarly offers Advanced Search for Bronze and above.

We chronicled our journey of deploying Elasticsearch for GitLab.com through several blog posts.
* [2019-03-20 Lessons from our journey to enable global code search with Elasticsearch on GitLab.com](/blog/2019/03/20/enabling-global-search-elasticsearch-gitlab-com/)
* [2019-07-16 Update: The challenge of enabling Elasticsearch on GitLab.com](/releases/2019/07/16/elasticsearch-update/)
* [2020-04-28 Update: Elasticsearch lessons learnt for Advanced Search](/blog/2020/04/28/elasticsearch-update/)

### Target Audience and Experience
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category. An overview
of the evolving user journeys as the category progresses through minimal, viable, complete and lovable maturity levels.-->

Advanced Search is targeted at all personas who might use GitLab. However, the largest benefits come to users performing cross-project code search looking for inner sourcing opportunities or exploring the breadth of public projects across GitLab.com.

GitLab is growing and the path to delivering a world-class Git Repo search is evolving quickly. Enterprise Edition Self-Managed customers need to provide their own install of Elasticsearch to connect to GitLab. We will need to be very creative about how to advance Community Edition and GitLab.com Free. Contributions are welcome!

### Maturity

Currently, GitLab's maturity for Search is viable. Here's why:

GitLab's current Advanced Search experience works for some self-managed instances and the experience of getting started has continued to improve.
The UI lacks some basic search capabilities. Using the search generally requires the user to be very exact in what they are looking for. It offers little capability to explore what is in a repo. We have Added Several of these in the past few months and expect to close the gap this year. 

### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top priorities for a few stakeholders. This section must provide a link to an issue or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

We have added a live roadmap that will track the progress as we complete major epics.
[Global Search Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aglobal%20search&label_name[]=Roadmap)

**In Progress**: [UX Search Enhancements for Advanced Search](https://gitlab.com/groups/gitlab-org/-/epics/1505) Our old user interface was designed to provide a minimal amount of complexity as we focused on building and scaling the backend components. We have started adding more rich features that are available with Elasticsearch. I expect that once we have these features available we will do another search UX design that better utilizes these features with a more complete User Flow.
Our customers have told us that having a single tab for all results with filtering to specific scopes would be a better approach to help find content types across the repo. We are also going to start using horizontal space better by adding filtering to the left-hand side of the search page. This will allow us to expand the results list as well as move more results above the fold. 

**Next**: [Advanced Search Ranking](https://gitlab.com/groups/gitlab-org/-/epics/3729) We have been using very little customizations to Ranking. Improving this will be key to the making finding information in GitLab less intensive. We are also going to [make indexing more efficient an improve the performance of Elasticsearch](https://gitlab.com/gitlab-org/gitlab/-/issues/234046).

### What is Not Planned right now

Currently there is not a plan to scale beyond the needs of Paid Groups on GitLab.com. This means that while the ambition of the Search Group is to expand Advanced Search to all users of GitLab, we're not yet ready to move in that direction. We will continue to add features to Basic search as we can while evolving Advanced Search. 

### The Future of Global Search

As GitLab continues to grow it will become important to keep growing with GitLab. One of these areas that Global search will have impact is in adding new content types. Currently, this includes adding [Vulnerabilities](https://gitlab.com/gitlab-org/gitlab/-/issues/233733), and [Snippets](https://gitlab.com/gitlab-org/gitlab/-/issues/20963). 

We will also be Allow be planning to offer deeper integration across other list pages in Gitlab. Lucene based indexing allows for very fast return of structured lists there are several pages in GitLab that offer these list and it’s a continual improvement to improve the speed. 

Increase the number of Self-Managed users, using Advanced Search. We are considering how to enable [Advanced Search by default](https://gitlab.com/gitlab-com/Product/-/issues/1716). We have already made Advanced Search part of SaaS for all paid users. 

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk about, but we’re talking with customers about what they actually use, and ultimately what they need.-->

Both GitHub and BitBucket provide a more comprehensive and complete search for users; particularly in their ability to deeply search code and surface those results to users. While GitLab's Advanced Search is available to self-managed users.

There is a great need to improve the ability to search across repos for all competitors. This is commonly asked about from our prospective customers and is included in the comparison matrix for GitLab as well as the ROI calculator. 

| Feature 	| GitLab 	| SourceGraph 	| GitHub 	| BitBucket 	| JIRA 	|
|-	|-	|-	|-	|-	|-	|
| Keyword Search <br>   Search with keywords not specifying a field 	| **{check-circle}** Yes 	| **{check-circle}** Yes 	| **{check-circle}** Yes 	|  	| **{dotted-circle}** No 	|
| Search Filters<br>   Filters can reduce results to more specific results 	| **{check-circle}** Yes 	| **{check-circle}** Yes 	| **{check-circle}** Yes 	|  	| **{check-circle}** Yes 	|
| NLP (Natural Language Processing)<br>   Search uses Linguistic structure to improve matching 	| **{check-circle}** Yes 	| **{check-circle}** Yes 	| **{check-circle}** Yes 	|  	| **{dotted-circle}** No 	|
| Language Code Filters <br>   Search detects Code Language and allows filtering to a specific Language 	| **{dotted-circle}** No 	| **{check-circle}** Yes 	| **{check-circle}** Yes 	|  	|  	|
| Search Diffs <br>   Search returns results from previous versions 	| **{dotted-circle}** No 	| **{check-circle}** Yes 	|  	|  	|  	|
| Commit message <br>   Search by text used for the Commit message 	| **{check-circle}** Yes 	| **{check-circle}** Yes 	|  	|  	|  	|
| Saved searches <br>   The search with parameters can be saved and favorited to show later  	| **{dotted-circle}** No 	| **{check-circle}** Yes 	|  	|  	|  	|
| Custom filter groupings <br>   Combine filters and save them as a filter grouping 	| **{dotted-circle}** No 	| **{check-circle}** Yes 	|  	|  	|  	|
| Autocomplete <br>   Keywords are recommended while you are typing 	| **{check-circle}** Yes 	| **{check-circle}** Yes 	|  	|  	|  	|
| Rank statistics<br>   Results show a score variable between the results 	| **{dotted-circle}** No 	| **{check-circle}** Yes 	|  	|  	|  	|
| Multiple Branches <br>   Code search can show more than the main branch 	| **{dotted-circle}** No 	| **{check-circle}** Yes 	|  	|  	|  	|
| Compare results <br>   Result page allows you to compare a result to other results from other searches 	| **{dotted-circle}** No 	| **{dotted-circle}** No 	|  	|  	|  	|
| Comments <br>   Results are shown from comments 	| **{check-circle}** Yes 	| **{dotted-circle}** No 	|  	|  	|  	|
| All Results <br>  Results are shown across all scopes or types in one integrated result page 	| **{dotted-circle}** No 	| **{check-circle}** Yes 	|  	|  	|  	|
| Keyboard Shortcuts <br>   Search is quickly accessible for key commands 	| **{check-circle}** Yes 	| **{check-circle}** Yes 	|  	|  	|  	|
| ID Quick Search <br>   Search Recognizes IDs and takes you directly to the item page 	| **{check-circle}** Yes 	| **{dotted-circle}** No 	|  	|  	|  	|
| Search by SHA<br>   Commit SHAs are recognized and you are directed to the sha instead of the results 	| **{check-circle}** Yes 	| **{dotted-circle}** No 	|  	|  	|  	|
| File Search <br>   Files and directories are identified and searchable not just the reference of a file in a file 	| **{check-circle}** Yes 	| **{dotted-circle}** No 	|  	|  	|  	|
| SaaS <br>   Featurefull search is offered as part of the SaaS offering.  	| **{check-circle}** Yes 	| **{dotted-circle}** No 	|  	|  	|  	|
| APIs<br>   Users can query search using APIs directly 	| **{check-circle}** Yes 	| **{dotted-circle}** No 	|  	|  	|  	|
| Issues<br>   Issues are searchable 	| **{check-circle}** Yes 	| **{dotted-circle}** No 	|  	|  	|  	|
| Merge Request<br>   Merge requests are searchable (not just the code 	| **{check-circle}** Yes 	| **{dotted-circle}** No 	|  	|  	|  	|
| Wiki<br>   Wikis are searchable 	| **{check-circle}** Yes 	| **{dotted-circle}** No 	|  	|  	|  	|
| Epics<br>   Epics are searchable 	| **{check-circle}** Yes 	| **{dotted-circle}** No 	|  	|  	|  	|
| Security Vulnerability<br>   Search results for vulnerabilites 	| **{dotted-circle}** No 	| **{dotted-circle}** No 	|  	|  	|  	|
| Snipets<br>   Snipets are searchable 	| **{dotted-circle}** No 	| **{dotted-circle}** No 	|  	|  	|  	|
| Batch Changes<br>   Stare batch changes from a result list 	| **{dotted-circle}** No 	| **{check-circle}** Yes 	|  	|  	|  	|
| Search Across Groups<br>   Search across groups, Global Search 	| **{check-circle}** Yes 	| **{check-circle}** Yes 	|  	|  	|  	|
| COST <br>   Per seat license (avg) 	| Included 	| $6 	|  	|  	|  	|


<!-- ### Analyst Landscape -->
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues that will help us stay relevant from their perspective.-->

<!-- ### Top Customer Success/Sales issue(s) -->
<!-- These can be sourced from the CS/Sales top issue labels when available, internal surveys, or from your conversations with them.-->

### Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most thumbs-up), but you may have a different item coming out of customer calls.-->

- [Determine cause for gradual increase in Elasticsearch latency since 11-02-2020](https://gitlab.com/gitlab-org/gitlab/-/issues/292439)
- [UX Search Enhancements for Advanced Search](https://gitlab.com/groups/gitlab-org/-/epics/1505)
- [Split each search scope to it's own Index](https://gitlab.com/groups/gitlab-org/-/epics/5196)

### Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

- [Advanced Search Operational maturity on GitLab.com and EE](https://gitlab.com/groups/gitlab-org/-/epics/2132)

### Top Strategy Item(s)
<!-- What's the most important thing to move your vision forward?-->

- [Global Search Direction Page](https://gitlab.com/groups/gitlab-com/-/epics/1272)
- [Global Search Category Maturity](https://gitlab.com/groups/gitlab-com/-/epics/1271)
