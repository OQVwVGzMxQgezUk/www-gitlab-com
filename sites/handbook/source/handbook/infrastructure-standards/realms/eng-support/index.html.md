---
layout: handbook-page-toc
title: "Engineering Customer Support Realm"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Quick links

* [Global infrastructure standards](/handbook/infrastructure-standards/)
* [Global labels and tags](/handbook/infrastructure-standards/labels-tags/)
* [Infrastructure policies](/handbook/infrastructure-standards/policies/)
* [Infrastructure helpdesk](/handbook/infrastructure-standards/helpdesk/)

## Overview

This realm is for the engineering security team to deploy shared and team-specific infrastructure resources.

### Access requests

To request access to a group, please see [group access request tutorial](/handbook/infrastructure-standards/tutorials/groups/access-request/).

> For email authenticity security reasons, only GitLab issues or Slack messages to owners or counterparts are allowed for infrastructure requests.

### Realm Owners

| Name                 | GitLab.com Handle       | Group Role       | Job Title                                       |
|----------------------|-------------------------|------------------|-------------------------------------------------|
| TBD                  | `tbd`                   | Owner            | TBD                                             |

## Realm labels and tags

The [global labels/tags](/handbook/infrastructure-standards/labels-tags/) and [realm labels/tags](/handbook/infrastructure-standards/realms/eng-support/labels-tags/) should be applied to each resource.

## Realm Groups

Each infrastructure group has a shared GCP project and/or AWS account for group members.

If a group has not been implemented yet, please contact the realm owner for assistance. After a group is implemented, a separate handbook page is created with usage documentation.

| Group Name (AWS Account/GCP Project Name) | Usage Documentation (Empty cells are not implemented yet)                                                                       |
|-------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------|
| `eng-support-shared-infra`                | <!--[Group Docs](/handbook/infrastructure-standards/realms/eng-infra/groups/eng-support-shared-infra)-->                        |
| `eng-support-shared-services`             | <!--[Group Docs](/handbook/infrastructure-standards/realms/eng-infra/groups/eng-support-shared-services)-->                     |
| `eng-support-saas`                        | <!--[Group Docs](/handbook/infrastructure-standards/realms/eng-infra/groups/eng-support-saas)-->                                |
| `eng-support-self-managed`                | <!--[Group Docs](/handbook/infrastructure-standards/realms/eng-infra/groups/eng-support-self-managed)-->                        |
| `eng-support-americas`                    | <!--[Group Docs](/handbook/infrastructure-standards/realms/eng-infra/groups/eng-support-americas)-->                            |
| `eng-support-emea`                        | <!--[Group Docs](/handbook/infrastructure-standards/realms/eng-infra/groups/eng-support-emea)-->                                |
| `eng-support-apac`                        | <!--[Group Docs](/handbook/infrastructure-standards/realms/eng-infra/groups/eng-support-apac)-->                                |

## Usage guidelines

This is a placeholder for the realm owner to provide instructions on best practices and usage guidelines for this infrastructure.
